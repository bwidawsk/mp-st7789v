ST7789V_MOD_DIR := $(USERMOD_DIR)

SRC_USERMOD += \
	       $(ST7789V_MOD_DIR)/st7789v.c \
	       $(ST7789V_MOD_DIR)/micropython_st7789v.c

# We can add our module folder to include paths if needed
# This is not actually needed in this example.
CFLAGS_USERMOD += -I$(ST7789V_MOD_DIR) -DMODULE_ST7789V_ENABLED=1
