#include "py/obj.h"
#include "py/runtime.h"
#include "py/builtin.h"
#include "py/mphal.h"
#include "extmod/machine_spi.h"

mp_obj_t st7789v_make_new(const mp_obj_type_t *type,
			  size_t n_args,
			  size_t n_kw,
			  const mp_obj_t *all_args);

static const mp_obj_type_t st7789v = {
    {&mp_type_type},
    .name = MP_QSTR_ST7789V,
    .make_new = st7789v_make_new,
};

mp_obj_t st7789v_make_new(const mp_obj_type_t *type,
			  size_t n_args,
			  size_t n_kw,
			  const mp_obj_t *all_args)
{
	return mp_const_none;
}

static const mp_map_elem_t globaltab[] = {
    {MP_ROM_QSTR(MP_QSTR___name__), MP_OBJ_NEW_QSTR(MP_QSTR_st7789v)},
};

static MP_DEFINE_CONST_DICT(globals, globaltab);
const mp_obj_module_t module = {
    .base = {&mp_type_module},
    .globals = (mp_obj_dict_t *)&globals,
};

MP_REGISTER_MODULE(MP_QSTR_st7789v, module, MODULE_ST7789V_ENABLED);
