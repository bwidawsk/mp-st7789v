#include "py/obj.h"
#include "py/runtime.h"
#include "py/builtin.h"
#include "py/mphal.h"
#include "extmod/machine_spi.h"

#include "ST7789_Defines.h"

#define delay(d) mp_hal_delay_us(d * 1000)
#define end_tft_write(...)                                                     \
	do {                                                                   \
	} while (0)
#define begin_tft_write(...)                                                   \
	do {                                                                   \
	} while (0)

struct st7789v {
	mp_obj_base_t *spi;

	mp_hal_pin_obj_t cs;  /* GPIO hooked to CS */
	mp_hal_pin_obj_t dc;  /* GPIO hooked to DC */
	mp_hal_pin_obj_t rst; /* GPIO hooked to DC */
};

#define _pin(name)                                                             \
	static void name##_hi(struct st7789v *display)                         \
	{                                                                      \
		mp_hal_pin_write(display->name, 1);                            \
	}                                                                      \
	static void name##_lo(struct st7789v *display)                         \
	{                                                                      \
		mp_hal_pin_write(display->name, 0);                            \
	}                                                                      \
	static void assert_##name##_hi(struct st7789v *display)                \
	{                                                                      \
		assert(mp_hal_pin_read(display->name) == 1);                   \
	}                                                                      \
	static void assert_##name##_lo(struct st7789v *display)                \
	{                                                                      \
		assert(mp_hal_pin_read(display->name) == 0);                   \
	}

_pin(cs) _pin(dc) _pin(rst)

    static void spi_xfer_byte(struct st7789v *display, const uint8_t buf)
{
	mp_machine_spi_p_t *spi =
	    (mp_machine_spi_p_t *)display->spi->type->protocol;
	spi->transfer(display->spi, 1, &buf, NULL);
}

/* 8.4.2
 * The write mode of the interface means the micro controller writes commands
 * and data to the LCD driver. 3-lines serial data packet contains a control bit
 * D/CX and a transmission byte. In 4-lines serial interface, data packet
 * contains just transmission byte and control bit D/CX is transferred by the
 * D/CX pin. If D/CX is “low”, the transmission byte is interpreted as a command
 * byte. If D/CX is “high”, the transmission byte is stored in the display data
 * RAM (memory write command), or command register as parameter.
*/

/*
 * Write the command to the given display via SPI.
 *
 * FIXME: If MISO isn't hooked up, we shouldn't allow commands that read
 * back data from the device */
static void eSPI_writecommand(struct st7789v *display, uint8_t cmd)
{
	assert_cs_hi(display);

	cs_lo(display);
	spi_xfer_byte(display, cmd);
	cs_hi(display);
}
#define writecommand(cmd) eSPI_writecommand(&display, cmd)

static void eSPI_writedata(struct st7789v *display, uint8_t data)
{
	assert_cs_hi(display);

	cs_lo(display);
	dc_hi(display);
	spi_xfer_byte(display, data);
	cs_hi(display);
}
#define writedata(data) eSPI_writedata(&display, data)

static void write_cmd(struct st7789v *display, const uint8_t cmd,
		      const uint8_t *data, size_t len)
{
	assert_cs_hi(display);

	cs_lo(display);
	dc_lo(display);
	spi_xfer_byte(display, cmd);
	/* Command without parameters */
	if (!len)
		goto out;

	dc_hi(display);
	for (int i = 0; i < len; i++) {
		spi_xfer_byte(display, data[i]);
	}

out:
	cs_hi(display);
}

/* https://github.com/Bodmer/TFT_eSPI/blob/0e0fd75277c19d834318ed509ad53466e418401d/TFT_Drivers/ST7789_Init.h */
static void eSPI_init()
{
	struct st7789v display;
	writecommand(ST7789_SLPOUT); // Sleep out
	delay(120);

	writecommand(ST7789_NORON); // Normal display mode on

	//------------------------------display and color format setting--------------------------------//
	writecommand(ST7789_MADCTL);
	//writedata(0x00);
	writedata(TFT_MAD_COLOR_ORDER);

	// JLX240 display datasheet
	writecommand(0xB6);
	writedata(0x0A);
	writedata(0x82);

	writecommand(ST7789_COLMOD);
	writedata(0x55);
	delay(10);

	//--------------------------------ST7789V Frame rate setting----------------------------------//
	writecommand(ST7789_PORCTRL);
	writedata(0x0c);
	writedata(0x0c);
	writedata(0x00);
	writedata(0x33);
	writedata(0x33);

	writecommand(ST7789_GCTRL); // Voltages: VGH / VGL
	writedata(0x35);

	//---------------------------------ST7789V Power setting--------------------------------------//
	writecommand(ST7789_VCOMS);
	writedata(0x28); // JLX240 display datasheet

	writecommand(ST7789_LCMCTRL);
	writedata(0x0C);

	writecommand(ST7789_VDVVRHEN);
	writedata(0x01);
	writedata(0xFF);

	writecommand(ST7789_VRHS); // voltage VRHS
	writedata(0x10);

	writecommand(ST7789_VDVSET);
	writedata(0x20);

	writecommand(ST7789_FRCTR2);
	writedata(0x0f);

	writecommand(ST7789_PWCTRL1);
	writedata(0xa4);
	writedata(0xa1);

	//--------------------------------ST7789V gamma setting---------------------------------------//
	writecommand(ST7789_PVGAMCTRL);
	writedata(0xd0);
	writedata(0x00);
	writedata(0x02);
	writedata(0x07);
	writedata(0x0a);
	writedata(0x28);
	writedata(0x32);
	writedata(0x44);
	writedata(0x42);
	writedata(0x06);
	writedata(0x0e);
	writedata(0x12);
	writedata(0x14);
	writedata(0x17);

	writecommand(ST7789_NVGAMCTRL);
	writedata(0xd0);
	writedata(0x00);
	writedata(0x02);
	writedata(0x07);
	writedata(0x0a);
	writedata(0x28);
	writedata(0x31);
	writedata(0x54);
	writedata(0x47);
	writedata(0x0e);
	writedata(0x1c);
	writedata(0x17);
	writedata(0x1b);
	writedata(0x1e);

	writecommand(ST7789_INVON);

	writecommand(ST7789_CASET); // Column address set
	writedata(0x00);
	writedata(0x00);
	writedata(0x00);
	writedata(0xE5); // 239

	writecommand(ST7789_RASET); // Row address set
	writedata(0x00);
	writedata(0x00);
	writedata(0x01);
	writedata(0x3F); // 319

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	end_tft_write();
	delay(120);
	begin_tft_write();

	writecommand(ST7789_DISPON); //Display on
	delay(120);

#ifdef TFT_BL
	// Turn on the back-light LED
	digitalWrite(TFT_BL, HIGH);
	pinMode(TFT_BL, OUTPUT);
#endif
}
